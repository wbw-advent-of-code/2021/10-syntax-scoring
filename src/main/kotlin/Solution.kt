import java.io.File
import java.util.*



fun main() {
    val file = "src/main/resources/input.txt"
    println(part1(file))
    println(part2(file))
}

fun part1(file: String): Long {
    return File(file).useLines { it.toList() }.sumOf { lineScore(it) }
}

fun part2(file: String): Long {
    val scores = File(file).useLines { it.toList() }
        .filter { lineScore(it) == 0L }
        .map { autoComplete(it) }
        .sorted()
    return scores[scores.size / 2]

}

fun lineScore(line: String): Long {
    val brackets = listOf(
        Triple('(', ')', 3L),
        Triple('[', ']', 57L),
        Triple('{', '}', 1197L),
        Triple('<', '>', 25137L)
    )

    val stack = Stack<Char>()

    line.toCharArray().forEach {
        when (it) {
            in (brackets.map { b -> b.first }) -> stack.push(it)
            in (brackets.map { b -> b.second }) -> {
                if (stack.empty() || brackets.single { b -> b.first == stack.peek() }.second != it) {
                    return brackets.filter { b -> b.second == it }
                        .map { b -> b.third }
                        .single()
                } else {
                    stack.pop()
                }
            }
        }
    }

    return 0
}

fun autoComplete(line: String): Long {
    val brackets = listOf(
        Triple('(', ')', 1L),
        Triple('[', ']', 2L),
        Triple('{', '}', 3L),
        Triple('<', '>', 4L)
    )

    var score = 0L
    val stack = Stack<Char>()

    line.toCharArray().forEach {
        when (it) {
            in (brackets.map { b -> b.first }) -> stack.push(it)
            in (brackets.map { b -> b.second }) -> {
                if (stack.isNotEmpty() && brackets.single { b -> b.first == stack.peek() }.second == it) {
                    stack.pop()
                }
            }
        }
    }

    /*

    }}]])})] - 288957 total points.
    )}>]}) - 5566 total points.
    }}>}>)))) - 1480781 total points.
    ]]}}]}]}> - 995444 total points.
    ])}> - 294 total points.

    */
    stack.toList()
        .reversed()
        .forEach {
            score *= 5
            score += brackets.single { b -> b.first == it }.third
        }

    return score
}