import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class SolutionKtTest {
    val file = "src/test/resources/input.txt"

    @Test
    internal fun `test part 1`() {
        assertThat(part1(file)).isEqualTo(26397)
    }

    @Test
    internal fun `test part 2`() {
        assertThat(part2(file)).isEqualTo(288957)
    }
}